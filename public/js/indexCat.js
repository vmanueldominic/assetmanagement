$(document).ready(function () {
	$('#editModal').on('show.bs.modal', function (event) {
	  let button = $(event.relatedTarget); // Button that triggered the modal
	  let id = button.data('id');
	  let title = button.data('title');
	  let description = button.data('description');
	  let code_name = button.data('code_name');

	   // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  let modal = $(this);
	 
	  modal.find('.modal-header h5').text("Edit " + title);
	  modal.find('.modal-body #name').val(title);
	  modal.find('.modal-body #description').val(description);
	  modal.find('.modal-body #code_name').val(code_name);

	  $('#editForm').attr('action', '/categories/' + id);
	});

	$('#addQtyModal').on('show.bs.modal', function (event) {
	  let button = $(event.relatedTarget); // Button that triggered the modal
	  let id = button.data('id');
	  let name = button.data('name');

	  let modal = $(this);
	 
	  modal.find('.modal-body #category_id').val(id);
	  modal.find('.modal-body #name').val(name);
	});

	$('#requestModal').on('show.bs.modal', function (event) {
	  let button = $(event.relatedTarget); // Button that triggered the modal
	  let id = button.data('id');
	  let name = button.data('name');

	  let modal = $(this);
	  modal.find('.modal-header h5').text("Request form for " + name);
	  modal.find('.modal-body #category_id').val(id);
	  // dateDiff();
	});
});


let today_date = new Date();
let todayTime = today_date.getTime();

$('#borrow_date').on('change',function(){
	let borrow_date = new Date($('#borrow_date').val());
	let borrowTime = borrow_date.getTime();

	if ((borrowTime - todayTime) >= 0) {
		$('#return_date').removeAttr("readonly");
		}else{
			alert('Please make sure input a valid Date!');
			$('#return_date').attr("readonly", true);
		}
	});

$('#return_date').on('change',function(){
	let borrow_date = new Date($('#borrow_date').val());
	let borrowTime = borrow_date.getTime();
	
	let return_date = new Date($('#return_date').val());
	let returnTime = return_date.getTime();
	
	let dateDifference = returnTime - borrowTime;

	if ((borrowTime - todayTime) >= 0) {
		if (dateDifference > 0) {
			$('#requestButton').removeAttr("disabled");
			$('#requestButton').removeClass("btn-secondary");
			$('#requestButton').addClass("btn-primary");
		}else{
			$('#requestButton').attr("disabled", true);
			$('#requestButton').addClass("btn-secondary");
			$('#requestButton').removeClass("btn-primary");
			alert('Please make sure that your Return Date is longer than your Borrow Date');
		}

	}else{
		$('#requestButton').attr("disabled", true);
		$('#requestButton').addClass("btn-secondary");
		$('#requestButton').removeClass("btn-primary");
		alert('Please make sure that your Return Date is longer than your Borrowed Date');
	}
	});


	



	// if (borrowTime >= todayTime && returnTime >= todayTime) {
	// 	if (dateDifference > 0) {
	// 		$('#requestButton').removeClass('disabled');
	// 	}else{
	// 		alert('Please make sure that the Return date is longer than the Borrow Date!');
	// 		$('#requestButton').addClass('disabled');
	// 	}
	// }else{
	// 	alert('Please make sure that both dates are higher than the current date!');
	// 	$('#requestButton').addClass('disabled');
	// }

