const addRow = $('#addRow');
const tbody = $('#newTRow');
const removeSelection = $('.remove');


$(document).on('blur', 'input',function()
{
	// $(this).get(0).files.length == 0 //for files
    if( $(this).val() == '') {
        $(this).addClass('border border-danger');
    }else{
    	$(this).removeClass('border border-danger');
    }
});


// button that links to the plus sign of the category thead to add new rows
addRow.on('click', function(){
	newCat();
	
})

// button performing the delete row
removeSelection.live('click', function(){
	let rows = $('#newTRow tr');
	if (rows.length == 1) {
		alert("Unable to delete the last row");
	}else{
		$(this).parent().parent().remove();
	}
})

// function that adds new category row
let index = 1;
function newCat() {
	let name = '<td><input class="form-control" type="text" name="name['+index+']" id="name['+index+']" required><label for="name['+index+'] class="font-weight-bold font-italic text-danger text-center test"></label></td>';
	let description = '<td><input class="form-control" type="text" name="description['+index+']" id="description['+index+']" required><label for="description['+index+'] class="font-weight-bold font-italic text-danger text-center test"></label></td>';
	let code_name = '<td><input class="form-control" type="text" name="code_name['+index+']" id="code_name['+index+']" required><label for="code_name['+index+'] class="font-weight-bold font-italic text-danger text-center test"></label></td>';
	let image = '<td><input class="form-control" type="file" name="image['+index+']" id="image['+index +']" accept="image/png, .jpeg, .jpg"  required><label for="image['+index+'] class="font-weight-bold font-italic text-danger text-center test"></label></td>';
	let remove = '<td class="text-center"><a href="#" class="btn btn-danger remove"><i class="fa fa-times" aria-hidden="true"></i></a></td>';
	let tr = '<tr>' + name + description + code_name + image + remove + '</tr>';
	let rows = $('#newTRow tr');
	if (rows.length < 10) {
		tbody.append(tr);
		index ++;	
	}else{
		alert("10 is the maximum rows");
	}
}
