<?php

namespace App\Http\Controllers;
use App\Asset;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Category::class);
         $categories = Category::paginate(5);
        // $assets = Asset::all();

         $isavailable[] = 0;
         $isunavailable[] = 0;

         $asset_code = Asset::firstWhere('isAvailable', 1);
         foreach ($categories as $category) {
            $available = 0;
            $unavailable = 0;
            foreach ($category->assets as $asset) {
                if ($asset->isAvailable == 1) {
                    $available++;
                }else{
                    $unavailable++;
                }
            }

                $isavailable[] = $available;
                $isunavailable[] = $unavailable;
            }
            
        return view('categories.index')
            ->with('categories', $categories)
            ->with(array('isavailable'=> $isavailable))
            ->with(array('isunavailable'=> $isunavailable))
            ->with('asset_code', $asset_code);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Category::class);
        $categories = Category::all();

        return view('categories.create')
        ->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Category::class);
        // call the validate() method on our request to apply validation rules on specified request input
        
        // test if $request is being counted
        // dd(count($request->toArray()));
        
        // validation process to make sure that the values are correct
        // dd(count($request->all()))
         $validator = Validator::make($request->all(),[
             'name.*' => 'required|string|unique:categories,name',
             'description.*' => 'required|string',
             'code_name.*' => 'required|string|unique:categories,code_name',
             'image.*' => 'required|image'
         ]);

         if ($validator->fails()) {
            return redirect('/categories/create')
                        ->withErrors($validator)
                        ->withInput();
        }else{
          if (count($request->name) > 0) {
            for ($index = 0; $index < count($request->name) ; $index++) { 

              $name = htmlspecialchars($request->name[$index]);
              $description = htmlspecialchars($request->description[$index]);
              $code_name = htmlspecialchars($request->code_name[$index]);
              $image = $request->file('image');

              // instantiate a new product object from the Product model
              $category = new Category;
              // set the properties of this object as defined in its migration to their respective sanitized form input values
              $category->name = $name;
              $category->description = $description;
              $category->code_name = $code_name;



              // handle the file image upload
              // set the file name of the uploaded image to be the time of upload, retaining the original file type
              $file_name = $code_name . "." . $image[$index]->getClientOriginalExtension();
              // set target destination where the file will be saved in
              $destination = "images/";
              // call the move() method of the $image object to save the uploaded file in the target destination under the specified file name
              $image[$index]->move($destination, $file_name);
              // set the path of the saved image as the value for the column img_path of this record
              $category->img_Path = $destination.$file_name;
              dd($file_name);
              // save the new product object as a new record in the products table via its save() method
              // $category->save();

            }
            // this will now call the index action of the ProductController as per LaraveL's resourceful routes

           $request->session()->flash('status', "New Categories Added!");
           return redirect('/categories/create');
          }else{
            $request->session()->flash('status', "Empty input");
            return redirect('/categories/create');
          }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('categories.show')->with('category', $category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $this->authorize('create', Category::class);
       $categories = Category::all();
       return view('categories.edit')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
      $this->authorize('update', Category::class);
      $request->validate([
          'name' => 'required|string',
          'description' => 'required|string',
          'code_name' => 'string',
          'image' => 'image'
      ]);

      $name = htmlspecialchars($request->input('name'));
      $description = htmlspecialchars($request->input('description'));
      $code_name = htmlspecialchars($request->input('code_name'));

      $duplicate = Category::where(strtolower('name'), strtolower($name))->first();
      if ($duplicate == null) {
          
      }
      // $category is the category object to be edited, this was obtained via LaraveL's route-model binding
      // overwrite the properties of $category with the input values from the edit form
      $category->name = $name;
      $category->description = $description;
      $category->code_name = $code_name;
    
      // if an image file upload is found, replace the current image of the product with the new upload
      if ($request->file('image') != null) {
          $image = $request->file('image');
          // set the file name of the uploaded image to be the time of upload, retaining the original file type
          $file_name = $code_name . "." . $image->getClientOriginalExtension();
          // set target destination where the file will be saved in
          $destination = "images/";
          // call the move() method of the $image object to save the uploaded file in the target destination under the specified file name
          $image->move($destination, $file_name);
          // set the path of the saved image as the value for the column img_path of this record
          $category->img_Path = $destination.$file_name;
      }

      if($category->save()){
         $request->session()->flash('status', "Asset Updated!");
      }
      return redirect("/categories");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if($category->isActive == 1){
          $category->isActive = 0;
        }else{
          $category->isActive = 1;
        }
        $category->save();

        return redirect('/categories');
    }
}
