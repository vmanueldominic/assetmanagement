<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Category;
use App\Asset;
use Illuminate\Http\Request;
use Auth;
use Session;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', Transaction::class);
        if (Auth::user()->isAdmin === 1) {
            $transactions = Transaction::all();
        }else{
            $transactions = Transaction::where('user_id', Auth::user()->id)->get();
        }

       
        return view('/transactions.index')
        ->with('transactions', $transactions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
        $this->authorize('create', Transaction::class);
        // call the validate() method on our request to apply validation rules on specified request input
        $request->validate([
            'borrow_date' => 'required|date',
            'return_date' => 'required|date'
        ]);

        $user_id = htmlspecialchars($request->input('user_id'));
        $category_id = htmlspecialchars($request->input('category_id'));
        $borrow_date = $request->input('borrow_date');
        $return_date = $request->input('return_date');
        

        $status_id = 1;
        // instantiate a new product object from the Product model
        $transaction = new Transaction;
            
        $transaction->user_id = $user_id;
        $transaction->status_id = $status_id;
        $transaction->category_id = $category_id;
        $transaction->borrow_date = $borrow_date;
        $transaction->return_date = $return_date;
        $transaction->damage = false;

        if ($transaction->save()) {
           $request->session()->flash('status', "Your request has been submitted");
           return redirect('/transactions');
        }


       
        // this will now call the index action of the ProductController as per LaraveL's resourceful routes

        //         if($transaction->save()){
        // 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {

        $this->authorize('update', Transaction::class);
        

        $asset = Asset::where('category_id', $transaction->category_id)
                ->where('isAvailable', 1)->first(); 
        
        $isdamaged = htmlspecialchars($request->input('isDamaged'));
        $comment = htmlspecialchars($request->input('comment'));

        $requested = 1;
        $approved = 2;
        $rejected = 3;
        $returned = 4;
        $cancelled = 5;

         if (Auth::user()->isAdmin === 1) {      
             if ($transaction->status_id == $requested) {
                 if ($asset) {
                     $transaction->asset_id = $asset->id;
                     $transaction->status_id = $approved;
                     
                     if($transaction->save()){   
                         $asset->isAvailable = false;
                         $asset->save();
                         $request->session()->flash('status', "The request has been APPROVED!");
                     } 
                 }else{
                     $transaction->status_id = $rejected;
                     $transaction->save();
                     $request->session()->flash('status', "The request has been REJECTED.");
                 }
             }else {
               $assetavailable = Asset::where('category_id', $transaction->category_id)->first();
               if ($isdamaged == "true") {
                    $request->validate([
                      'comment' => 'required|string'
                    ]);
                    $transaction->status_id = $returned;
                    $transaction->damage = true;
                    $transaction->comment = $comment;

                    if($transaction->save()){
                        $assetavailable->isAvailable = false;
                        $assetavailable->save();
                        $request->session()->flash('status', "The asset has been returned DAMAGED.");
                    }
               }else{
                    $transaction->status_id = $returned;
                    $transaction->damage = false;
                    $transaction->comment = $comment;

                    if($transaction->save()){
                        $assetavailable->isAvailable = true;
                        $assetavailable->save();
                        $request->session()->flash('status', "The asset has been returned SUCCESSFULLY!");
                    } 
               }
             }
            
          }

        return redirect("/transactions");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Transaction $transaction)
    {

        $requested = 1;
        $approved = 2;
        $rejected = 3;
        $returned = 4;
        $cancelled = 5;


          if (Auth::user()->isAdmin === 1) {      
              $transaction->status_id = $rejected;
              $transaction->save();
              $request->session()->flash('status', "The request has been REJECTED.");
          }else{
              $transaction->status_id = $cancelled;
              $transaction->save();
              $request->session()->flash('status', "You cancelled your request.");
          }

        return redirect("/transactions");
    }
}
