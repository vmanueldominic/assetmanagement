<?php

namespace App\Http\Controllers;
use App\Category;
use App\Asset;
use App\Transaction;
use Illuminate\Http\Request;
use Session;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Category::class);
        $categories = Category::all();

        return view('assets.create')
        ->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //dd($request->input('category'));
       $this->authorize('create', Asset::class);

       $validator = Validator::make($request->all(),[
           'category.*' => 'required',
           'quantity.*' => 'required'
       ]);

        if ($validator->fails()) {
           return redirect('/assets/create')
                       ->withErrors($validator)
                       ->withInput();
       }else{
          if (count($request->category) > 0) {
            // $countArray[] = 0;
            // $catArray[] = "";
            for ($index = 0; $index < count($request->category); $index++) { 

              $category_id = htmlspecialchars($request->category[$index]);
              $quantity = htmlspecialchars($request->quantity[$index]);
              
              $category = Category::find($category_id);
              for ($counter = 1; $counter <= $quantity; $counter ++) { 
                  $asset = new Asset;
                  $asset->serial_code = $category->code_name.time()."-".$counter;
                  $asset->category_id = $category_id; 
                  $asset->save();
              }
              $catArrays[] = $category->name;
              $countArrays[] = $quantity;
              
            }
           $request->session()->flash('status', "Asset(s) added successfully");
           return redirect('/categories')
           ->with(array('catArrays'=> $catArrays))
           ->with(array('countArrays'=> $countArrays));

          }else{
            $request->session()->flash('status', "Empty input");
            return redirect('/assets/create');
          }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        $this->authorize('viewAny', Asset::class);

        $transactions[] = 0;
        $transactions = Transaction::where('asset_id', $asset->id)->get();
        $transaction = $transactions->flatten();
        // dd($transaction->count());
        return view('assets.show')
        ->with('asset', $asset)
        ->with(array('transactions'=> $transaction));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
        if($asset->isAvailable == 1){
          $asset->isAvailable = 0;
        }else{
          $asset->isAvailable = 1;
        }
        $asset->save();



        return view('categories.show')
        ->with('asset', $asset->category_id)->with('category', $asset->category);
    }
}
