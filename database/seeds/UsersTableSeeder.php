<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$vWieO1qXqPCgUym3uQ.AzuB3HilxCRxSbEMNX1fjkKo6BeAwDnKI.',
                'isAdmin' => 1,
                'remember_token' => NULL,
                'created_at' => '2020-04-25 00:50:53',
                'updated_at' => '2020-04-25 00:50:53',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'user1',
                'email' => 'user1@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$gqlEjX5MjAwR6oCF/JlZhe9szhbK/Kymch7PM7C3SdV93LYKbUC0.',
                'isAdmin' => 0,
                'remember_token' => NULL,
                'created_at' => '2020-04-25 00:55:28',
                'updated_at' => '2020-04-25 00:55:28',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'user2',
                'email' => 'user2@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$iEN24AORNBvykoBljqlHCOfnq62BzhiwUVdO9sIfSFJkLwehSVIpS',
                'isAdmin' => 0,
                'remember_token' => NULL,
                'created_at' => '2020-04-28 13:04:02',
                'updated_at' => '2020-04-28 13:04:02',
            ),
        ));
        
        
    }
}