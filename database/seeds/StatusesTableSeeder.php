<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('statuses')->delete();
        
        \DB::table('statuses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Requested',
                'created_at' => '2020-04-27 19:44:34',
                'updated_at' => '2020-04-27 19:44:34',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Approved',
                'created_at' => '2020-04-27 19:45:14',
                'updated_at' => '2020-04-27 19:45:14',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Rejected',
                'created_at' => '2020-04-28 19:45:14',
                'updated_at' => '2020-04-27 19:45:14',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Returned',
                'created_at' => '2020-04-27 19:46:17',
                'updated_at' => '2020-04-27 19:46:17',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Cancelled',
                'created_at' => '2020-04-27 19:46:17',
                'updated_at' => '2020-04-27 19:46:17',
            ),
        ));
        
        
    }
}