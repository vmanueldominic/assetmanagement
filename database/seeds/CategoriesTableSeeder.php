<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Acer Chromebook 714',
                'description' => 'Acer Chromebook 714 is a Chrome OS laptop with a 14.00-inch display that has a resolution of 1080x1920 pixels. it comes with 8GB of RAM. Connectivity options include Wi-Fi 802.11 a/b/g/n/ac, Bluetooth and it comes with Multi Card Slot ports.',
                'img_Path' => 'images/1593959261.png',
                'code_name' => 'ACRCHB714',
                'isActive' => 1,
                'created_at' => '2020-04-25 01:00:16',
                'updated_at' => '2020-07-05 14:27:41',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Asus tuf gaming fx505',
            'description' => 'Processor : 2.3 GHz Intel Core i5-8300H (8M Cache, up to 3.9 GHz) 8th Gen processor Display : 15.6-inch (16:9) LED-backlit FHD (1920x1080) 60Hz Anti-Glare IPS-level Panel with 45% NTSC. Memory &amp; Storage : 8GB DDR4 RAM with NVIDIA GeForce GTX 1050 Ti GDDR5 4GB Graphics | Storage : 1TB 5400 rpm Hybrid HDD (FireCuda) with PCIE NVME 256GB M.2 SSD OS : Windows 10 operating System | Weight : 2.30kg laptop Keyboard : Highlighted WASD keys | Desktop-inspired Design keyboard |Red Illuminated chiclet keyboard | lifespan of 20 million key press|0.25mm keycap curves |1.8mm of Key travel Distance. Hypercool Technology : Anti-Dust Cooling For Extended Lifespan | Powerful Dual Fan Architecture | Fan Overboost Technology | Patended Thermal-Friendly Chassis | Intelligent Keyboard Cooling. Audio : DTS Headphone:X | Authentic 7.1-Channel Surround Sound | Audophile-grade Equalizer Sound Options | Optimized Game/ Movie/ Sports Audio Profile. I/O PORTS : 1 x COMBO audio jack |1 x Type-A USB2.0 | 2 x Type-A USB 3.1 (Gen 1) | 1 x RJ45 LAN jack for LAN insert |1 x HDMI, HDMI support 1.4.',
                'img_Path' => 'images/1593959327.jpg',
                'code_name' => 'ASUSTGFX505',
                'isActive' => 1,
                'created_at' => '2020-04-25 01:15:17',
                'updated_at' => '2020-07-05 14:28:47',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'MacBook Pro 13-inch model',
            'description' => 'Apple MacBook Pro 13-inch 2020 is a macOS laptop with a 13.30-inch display that has a resolution of 1600x2560 pixels. It is powered by a Core i5 processor and it comes with 8GB of RAM. The Apple MacBook Pro 13-inch 2020 packs 256GB of SSD storage. Graphics are powered by Intel Integrated Iris Plus Graphics 645. Connectivity options include Wi-Fi 802.11 a/b/g/n/ac, Bluetooth and it comes with 2 USB ports (2 x Thunderbolt 3 (Type C)), Headphone and Mic Combo Jack ports.',
                'img_Path' => 'images/1593960047.jpg',
                'code_name' => 'MCBKP13',
                'isActive' => 1,
                'created_at' => '2020-04-25 01:16:02',
                'updated_at' => '2020-07-05 14:40:47',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Lenovo ThinkPad E590',
            'description' => 'RAM: 16GB DDR4 RAM; Storage: Samsung 256GB PCIe NVMe M.2 SSD (Seal is opened for upgrade ONLY, Professional Installation Service included) 15.6-Inch Anti-Glare (1366x768) Display with Front Facing HD Webcam | Integrated Intel UHD Graphics 620 - Supports external digital monitor via HDMI or USB Type-C; Supports dual independent display; Max resolution: 1920x1080 (HDMI)@60Hz Whiskey Lake-U CPU: Intel Quad Core i5-8265U 1.6 GHz (Turbo 3.90 GHz, 4 Cores 8 Threads, 6MB SmartCache) Intel 9260 11ac, 2x2 + BT5.0 Combo | HD Webcam',
                'img_Path' => 'images/1593960080.jpg',
                'code_name' => 'LNVTLLE590',
                'isActive' => 1,
                'created_at' => '2020-04-25 01:16:37',
                'updated_at' => '2020-07-05 14:41:20',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Predator Helios 300 Gaming Laptop - PH315-52-72RG',
            'description' => '2.4 GhzGHz Intel Core i5 9300H 9th Gen processor 8GB DDR4 RAM 1TB 7200rpm hard drive 15.6-inch screen, NVIDIA GeForce GTX 1650 4GB Graphics Windows 10 Home operating system 8 hours battery life, 2.3kg laptop Processor: Intel Core i5-9300H processor, turbo up to 4.10 Ghz, Display: 15.6&amp;quot; display with IPS FHD 1920 x 1080, (300 nits) 144 Hz, 3 ms',
                'img_Path' => 'images/1593960135.jpg',
                'code_name' => 'PRDTRH300',
                'isActive' => 1,
                'created_at' => '2020-04-29 15:06:59',
                'updated_at' => '2020-07-05 14:42:15',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Xiaomi Mi Gaming Laptop 2019',
            'description' => 'Xiaomi Mi Gaming Laptop 2019 is a Windows 10 Home laptop with a 15.60-inch display that has a resolution of 1920x1080 pixels. It is powered by a Intel Core processor and it comes with 8GB of RAM. The Xiaomi Mi Gaming Laptop 2019 packs 512GB of SSD storage. Graphics are powered by Nvidia. Connectivity options include Wi-Fi 802.11 ac, Ethernet and it comes with 4 USB ports (4 x USB 3.0), HDMI Port, Multi Card Slot, Headphone and Mic Combo Jack, Mic In, RJ45 (LAN) ports.',
                'img_Path' => 'images/1593962896.JPG',
                'code_name' => 'XMIGL2019',
                'isActive' => 1,
                'created_at' => '2020-07-05 15:17:51',
                'updated_at' => '2020-07-05 15:28:16',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'HP Envy x360 13-AG0035AU',
            'description' => 'HP Envy x360 13-AG0035AU is a Windows 10 Home laptop with a 13.30-inch display that has a resolution of 1920x1080 pixels. It is powered by a Ryzen 5 processor and it comes with 8GB of RAM. The HP Envy x360 13-AG0035AU packs 256GB of SSD storage. Connectivity options include Wi-Fi 802.11 a/b/g/n/ac and it comes with 3 USB ports (2 x USB 3.0 (Type A), 1 x USB 3.0 (Type C)), Multi Card Slot, Headphone and Mic Combo Jack ports.',
                'img_Path' => 'images/1593962918.JPG',
                'code_name' => 'HPENVY360',
                'isActive' => 1,
                'created_at' => '2020-07-05 15:17:51',
                'updated_at' => '2020-07-05 15:28:38',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Apple MacBook Air',
            'description' => 'Apple MacBook Air MREA2HN/A is a macOS laptop with a 13.30-inch display that has a resolution of 2560x1600 pixels. It is powered by a Core i5 processor and it comes with 8GB of RAM. The Apple MacBook Air MREA2HN/A packs 128GB of SSD storage. Graphics are powered by Intel Integrated HD Graphics. Connectivity options include Wi-Fi 802.11 ac, Bluetooth and it comes with 4 USB ports (2 x USB 2.0, 2 x USB 3.0), Multi Card Slot, Mic In ports.',
                'img_Path' => 'images/MCBKAIR.JPG',
                'code_name' => 'MCBKAIR',
                'isActive' => 1,
                'created_at' => '2020-07-05 15:17:51',
                'updated_at' => '2020-07-05 15:29:56',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Acer Nitro 5 AN515-51',
            'description' => 'Acer Nitro 5 AN515-51 is a Windows 10 Home laptop with a 15.60-inch display that has a resolution of 1920x1080 pixels. It is powered by a Core i5 processor and it comes with 8GB of RAM. The Acer Nitro 5 AN515-51 packs 128GB of HDD storage. Graphics are powered by Nvidia GeForce GTX 1050. Connectivity options include Wi-Fi 802.11 a/b/g/n/ac, Bluetooth, Ethernet and it comes with 4 USB ports (2 x USB 2.0, 1 x USB 3.0), Mic In, RJ45 (LAN) ports.',
                'img_Path' => 'images/1593962271.JPG',
                'code_name' => 'ACERNTRO5',
                'isActive' => 1,
                'created_at' => '2020-07-05 15:17:51',
                'updated_at' => '2020-07-05 15:17:51',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Dell Inspiron 5570',
                'description' => 'Dell Inspiron 5570 is a Windows 10 laptop with a 15.60-inch display that has a resolution of 1920x1080 pixels. It is powered by a Core i7 processor and it comes with 4GB of RAM. The Dell Inspiron 5570 packs 1TB of HDD storage. Graphics are powered by AMD Radeon 530 Graphics. Connectivity options include Wi-Fi 802.11 ac, Bluetooth and it comes with Mic In ports.',
                'img_Path' => 'images/1593962271.png',
                'code_name' => 'DELLINSPRN5570',
                'isActive' => 1,
                'created_at' => '2020-07-05 15:17:51',
                'updated_at' => '2020-07-05 15:17:51',
            ),
        ));
        
        
    }
}