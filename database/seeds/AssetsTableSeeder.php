<?php

use Illuminate\Database\Seeder;

class AssetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('assets')->delete();
        
        \DB::table('assets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'serial_code' => 'ACRCHB7141587776439-1',
                'isAvailable' => 0,
                'created_at' => '2020-04-25 01:00:39',
                'updated_at' => '2020-07-05 15:43:56',
                'category_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'serial_code' => 'ACRCHB7141587776439-2',
                'isAvailable' => 1,
                'created_at' => '2020-04-25 01:00:39',
                'updated_at' => '2020-04-28 13:16:10',
                'category_id' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'serial_code' => 'ACRCHB7141587776439-3',
                'isAvailable' => 1,
                'created_at' => '2020-04-25 01:00:39',
                'updated_at' => '2020-04-28 13:19:39',
                'category_id' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'serial_code' => 'ASUSTGFX5051587777330-1',
                'isAvailable' => 1,
                'created_at' => '2020-04-25 01:15:30',
                'updated_at' => '2020-04-28 12:12:09',
                'category_id' => 2,
            ),
            4 => 
            array (
                'id' => 5,
                'serial_code' => 'ASUSTGFX5051587777330-2',
                'isAvailable' => 1,
                'created_at' => '2020-04-25 01:15:30',
                'updated_at' => '2020-04-27 17:14:21',
                'category_id' => 2,
            ),
            5 => 
            array (
                'id' => 6,
                'serial_code' => 'ASUSTGFX5051587777330-3',
                'isAvailable' => 1,
                'created_at' => '2020-04-25 01:15:30',
                'updated_at' => '2020-04-28 10:35:10',
                'category_id' => 2,
            ),
            6 => 
            array (
                'id' => 7,
                'serial_code' => 'MCBKP131587777371-1',
                'isAvailable' => 0,
                'created_at' => '2020-04-25 01:16:11',
                'updated_at' => '2020-07-05 15:41:00',
                'category_id' => 3,
            ),
            7 => 
            array (
                'id' => 8,
                'serial_code' => 'MCBKP131587777371-2',
                'isAvailable' => 1,
                'created_at' => '2020-04-25 01:16:11',
                'updated_at' => '2020-04-25 01:16:11',
                'category_id' => 3,
            ),
            8 => 
            array (
                'id' => 9,
                'serial_code' => 'MCBKP131587777371-3',
                'isAvailable' => 1,
                'created_at' => '2020-04-25 01:16:11',
                'updated_at' => '2020-04-25 01:16:11',
                'category_id' => 3,
            ),
            9 => 
            array (
                'id' => 10,
                'serial_code' => 'MCBKP131587777371-4',
                'isAvailable' => 1,
                'created_at' => '2020-04-25 01:16:11',
                'updated_at' => '2020-04-25 01:16:11',
                'category_id' => 3,
            ),
            10 => 
            array (
                'id' => 11,
                'serial_code' => 'LNVTLLE5901587777403-1',
                'isAvailable' => 1,
                'created_at' => '2020-04-25 01:16:43',
                'updated_at' => '2020-07-05 15:41:09',
                'category_id' => 4,
            ),
            11 => 
            array (
                'id' => 12,
                'serial_code' => 'LNVTLLE5901587777403-2',
                'isAvailable' => 1,
                'created_at' => '2020-04-25 01:16:43',
                'updated_at' => '2020-04-25 01:16:43',
                'category_id' => 4,
            ),
            12 => 
            array (
                'id' => 13,
                'serial_code' => 'LNVTLLE5901587777403-3',
                'isAvailable' => 1,
                'created_at' => '2020-04-25 01:16:43',
                'updated_at' => '2020-04-25 01:16:43',
                'category_id' => 4,
            ),
            13 => 
            array (
                'id' => 14,
                'serial_code' => 'LNVTLLE5901587777403-4',
                'isAvailable' => 1,
                'created_at' => '2020-04-25 01:16:43',
                'updated_at' => '2020-04-25 01:16:43',
                'category_id' => 4,
            ),
            14 => 
            array (
                'id' => 15,
                'serial_code' => 'LNVTLLE5901587777403-5',
                'isAvailable' => 1,
                'created_at' => '2020-04-25 01:16:43',
                'updated_at' => '2020-04-25 01:16:43',
                'category_id' => 4,
            ),
            15 => 
            array (
                'id' => 16,
                'serial_code' => 'PRDTRH3001588173013-1',
                'isAvailable' => 0,
                'created_at' => '2020-04-29 15:10:13',
                'updated_at' => '2020-07-05 15:40:42',
                'category_id' => 5,
            ),
            16 => 
            array (
                'id' => 17,
                'serial_code' => 'PRDTRH3001588173013-2',
                'isAvailable' => 1,
                'created_at' => '2020-04-29 15:10:13',
                'updated_at' => '2020-04-29 15:10:13',
                'category_id' => 5,
            ),
            17 => 
            array (
                'id' => 18,
                'serial_code' => 'XMIGL20191593963056-1',
                'isAvailable' => 0,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:41:51',
                'category_id' => 6,
            ),
            18 => 
            array (
                'id' => 19,
                'serial_code' => 'XMIGL20191593963056-2',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 6,
            ),
            19 => 
            array (
                'id' => 20,
                'serial_code' => 'XMIGL20191593963056-3',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 6,
            ),
            20 => 
            array (
                'id' => 21,
                'serial_code' => 'XMIGL20191593963056-4',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 6,
            ),
            21 => 
            array (
                'id' => 22,
                'serial_code' => 'XMIGL20191593963056-5',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 6,
            ),
            22 => 
            array (
                'id' => 23,
                'serial_code' => 'HPENVY3601593963056-1',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 7,
            ),
            23 => 
            array (
                'id' => 24,
                'serial_code' => 'HPENVY3601593963056-2',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 7,
            ),
            24 => 
            array (
                'id' => 25,
                'serial_code' => 'HPENVY3601593963056-3',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 7,
            ),
            25 => 
            array (
                'id' => 26,
                'serial_code' => 'HPENVY3601593963056-4',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 7,
            ),
            26 => 
            array (
                'id' => 27,
                'serial_code' => 'HPENVY3601593963056-5',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 7,
            ),
            27 => 
            array (
                'id' => 28,
                'serial_code' => 'MCBKAIR1593963056-1',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 8,
            ),
            28 => 
            array (
                'id' => 29,
                'serial_code' => 'MCBKAIR1593963056-2',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 8,
            ),
            29 => 
            array (
                'id' => 30,
                'serial_code' => 'MCBKAIR1593963056-3',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 8,
            ),
            30 => 
            array (
                'id' => 31,
                'serial_code' => 'MCBKAIR1593963056-4',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 8,
            ),
            31 => 
            array (
                'id' => 32,
                'serial_code' => 'MCBKAIR1593963056-5',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 8,
            ),
            32 => 
            array (
                'id' => 33,
                'serial_code' => 'ACERNTRO51593963056-1',
                'isAvailable' => 0,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:40:44',
                'category_id' => 9,
            ),
            33 => 
            array (
                'id' => 34,
                'serial_code' => 'ACERNTRO51593963056-2',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 9,
            ),
            34 => 
            array (
                'id' => 35,
                'serial_code' => 'ACERNTRO51593963056-3',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 9,
            ),
            35 => 
            array (
                'id' => 36,
                'serial_code' => 'ACERNTRO51593963056-4',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 9,
            ),
            36 => 
            array (
                'id' => 37,
                'serial_code' => 'ACERNTRO51593963056-5',
                'isAvailable' => 1,
                'created_at' => '2020-07-05 15:30:56',
                'updated_at' => '2020-07-05 15:30:56',
                'category_id' => 9,
            ),
        ));
        
        
    }
}