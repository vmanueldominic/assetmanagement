<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transactions')->delete();
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 2,
                'status_id' => 5,
                'category_id' => 1,
                'asset_id' => NULL,
                'borrow_date' => '2020-07-06 00:00:00',
                'return_date' => '2020-07-08 00:00:00',
                'damage' => 0,
                'comment' => NULL,
                'created_at' => '2020-07-05 15:37:08',
                'updated_at' => '2020-07-05 15:38:02',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 2,
                'status_id' => 4,
                'category_id' => 4,
                'asset_id' => 11,
                'borrow_date' => '2020-07-06 00:00:00',
                'return_date' => '2020-07-10 00:00:00',
                'damage' => 0,
                'comment' => '',
                'created_at' => '2020-07-05 15:37:23',
                'updated_at' => '2020-07-05 15:41:09',
            ),
            2 => 
            array (
                'id' => 3,
                'user_id' => 2,
                'status_id' => 4,
                'category_id' => 6,
                'asset_id' => 18,
                'borrow_date' => '2020-07-06 00:00:00',
                'return_date' => '2020-07-10 00:00:00',
                'damage' => 1,
                'comment' => 'scratches on screen',
                'created_at' => '2020-07-05 15:37:43',
                'updated_at' => '2020-07-05 15:41:51',
            ),
            3 => 
            array (
                'id' => 4,
                'user_id' => 2,
                'status_id' => 3,
                'category_id' => 5,
                'asset_id' => NULL,
                'borrow_date' => '2020-07-07 00:00:00',
                'return_date' => '2020-07-31 00:00:00',
                'damage' => 0,
                'comment' => NULL,
                'created_at' => '2020-07-05 15:37:58',
                'updated_at' => '2020-07-05 15:40:46',
            ),
            4 => 
            array (
                'id' => 5,
                'user_id' => 3,
                'status_id' => 2,
                'category_id' => 3,
                'asset_id' => 7,
                'borrow_date' => '2020-07-08 00:00:00',
                'return_date' => '2020-07-25 00:00:00',
                'damage' => 0,
                'comment' => NULL,
                'created_at' => '2020-07-05 15:39:23',
                'updated_at' => '2020-07-05 15:41:00',
            ),
            5 => 
            array (
                'id' => 6,
                'user_id' => 3,
                'status_id' => 2,
                'category_id' => 5,
                'asset_id' => 16,
                'borrow_date' => '2020-07-08 00:00:00',
                'return_date' => '2020-07-25 00:00:00',
                'damage' => 0,
                'comment' => NULL,
                'created_at' => '2020-07-05 15:39:34',
                'updated_at' => '2020-07-05 15:40:42',
            ),
            6 => 
            array (
                'id' => 7,
                'user_id' => 3,
                'status_id' => 2,
                'category_id' => 9,
                'asset_id' => 33,
                'borrow_date' => '2020-07-09 00:00:00',
                'return_date' => '2020-07-22 00:00:00',
                'damage' => 0,
                'comment' => NULL,
                'created_at' => '2020-07-05 15:39:49',
                'updated_at' => '2020-07-05 15:40:44',
            ),
            7 => 
            array (
                'id' => 8,
                'user_id' => 3,
                'status_id' => 3,
                'category_id' => 8,
                'asset_id' => NULL,
                'borrow_date' => '2020-07-07 00:00:00',
                'return_date' => '2020-07-23 00:00:00',
                'damage' => 0,
                'comment' => NULL,
                'created_at' => '2020-07-05 15:40:03',
                'updated_at' => '2020-07-05 15:40:58',
            ),
            8 => 
            array (
                'id' => 9,
                'user_id' => 2,
                'status_id' => 2,
                'category_id' => 1,
                'asset_id' => 1,
                'borrow_date' => '2020-07-07 00:00:00',
                'return_date' => '2020-07-14 00:00:00',
                'damage' => 0,
                'comment' => NULL,
                'created_at' => '2020-07-05 15:43:09',
                'updated_at' => '2020-07-05 15:43:55',
            ),
            9 => 
            array (
                'id' => 10,
                'user_id' => 2,
                'status_id' => 1,
                'category_id' => 2,
                'asset_id' => NULL,
                'borrow_date' => '2020-07-16 00:00:00',
                'return_date' => '2020-07-28 00:00:00',
                'damage' => 0,
                'comment' => NULL,
                'created_at' => '2020-07-05 15:43:21',
                'updated_at' => '2020-07-05 15:43:21',
            ),
            10 => 
            array (
                'id' => 11,
                'user_id' => 2,
                'status_id' => 1,
                'category_id' => 4,
                'asset_id' => NULL,
                'borrow_date' => '2020-07-08 00:00:00',
                'return_date' => '2020-07-22 00:00:00',
                'damage' => 0,
                'comment' => NULL,
                'created_at' => '2020-07-05 15:43:31',
                'updated_at' => '2020-07-05 15:43:31',
            ),
        ));
        
        
    }
}