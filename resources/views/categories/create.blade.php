@extends('layouts.app')

@section('content')
	<form method="POST" action="/categories" enctype="multipart/form-data">
		@csrf
		<div class="row">
			<div class="col-md-8 offset-2">
				{{-- error checker --}}		
				<div>
					@if ($errors->any())
						<div class="alert alert-danger">
							<div>
								<button type="button" class="close" data-dismiss="alert">&times;</button>
							</div>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>			  
					@endif
				</div>
				{{-- end of error check --}}

				{{-- Create Nofications --}}
				@if(session('status') == "New Categories Added!")
					<div class="alert alert-success text-center">
						<strong>{{session('status')}}</strong>
					</div>
				@elseif(session('status') == "New asset(s) has been added.")
					<div class="alert alert-success text-center">
						<strong>{{session('status')}}</strong>
					</div>
				@endif
				{{-- End of Nofications --}}
				<table class="table table-bordered">
					<thead class="thead text-light">
						<tr class="text-center">
							<th>Name</th>
							<th>Description</th>
							<th>Code Name</th>
							<th>Image</th>
							<th><a href="#" id="addRow" class="text-primary"><i class="fa fa-plus-square" aria-hidden="true"></i></a>
							</th>
						</tr>
					</thead>
					<tbody id="newTRow">
						<tr>
							<td class="text-center"><input class="form-control" type="text" name="name[0]" id="name[0]" required><label id="lname[0]" class="font-weight-bold font-italic text-danger text-center test" ></label></td>
							<td><input class="form-control" type="text" name="description[0]" id="description[0]" required><label id="ldescription[0]" class="font-weight-bold font-italic text-danger text-center"></label></td>
							<td><input class="form-control" type="text" name="code_name[0]" id="code_name[0]" required><label id="lcode_name[0]" class="font-weight-bold font-italic text-danger text-center"></label></td>
							<td><input class="form-control" type="file" name="image[0]" id="image[0]" accept="image/png, .jpeg, .jpg" required><label id="limage[0]" class="font-weight-bold font-italic text-danger text-center"></label></td>
							<td class="text-center"><a href="#" class="btn btn-danger remove"><i class="fa fa-times" aria-hidden="true"></i></a></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td style="border: none"></td>
							<td style="border: none"></td>
							<td style="border: none"></td>
							<td style="border: none"></td>
							<td style="border: none" class="text-center"><button type="submit" class="btn btn-primary">Submit</button></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</form>
	<script src="{{asset('js/addCat.js')}}"></script>
@endsection





			