<div class="form-group">
	<label for="name">Name: </label>
	<input class="form-control" type="text" name="name" id="name">
</div>
<div class="form-group">
	<label for="description">Description: </label>
	<input class="form-control" type="text" name="description" id="description" value="">
</div>
<div class="form-group">
	<label for="code_name">Code Name: </label>
	<input class="form-control" type="text" name="code_name" id="code_name" value="" readonly>
</div>
<div class="form-group">
	<label for="image">Image:</label>
	<input class="form-control" type="file" name="image" id="image">
</div>