@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-sm-8 offset-2">
			{{-- call the Gate defined in AuthServiceProvider --}}
			@can('isAdmin')

				{{-- error checker --}}
				<div>
					@if ($errors->any())
						         <div class="alert alert-danger">
				  		         <div>
				  		         	<button type="button" class="close" data-dismiss="alert">&times;</button>
				  		         </div>
							            <ul>
							                @foreach ($errors->all() as $error)
							                    <li>{{ $error }}</li>
							                @endforeach
							            </ul>
				  		         </div>								     
					@endif
				</div>
				{{-- end of error check --}}

				{{-- Create Nofications --}}
				@if(session('status') == "Asset Updated!")
					<div class="alert alert-success text-center">
						<strong>{{session('status')}}</strong>
					</div>
				@elseif(session('status') == "Asset(s) added successfully")
					<div class="alert alert-success text-center">
						<strong>{{session('status')}}</strong>
						{{-- <ul>
							@foreach($catArrays as $array => $value) --}}
								{{-- <li>{{$array}}</li> --}}
{{-- 							@endforeach
						</ul> --}}
					</div>
				@endif
				{{-- End of Nofications --}}

				<div class="row">
					<div class="col-md-4">
						<h2>Categories</h2>
					</div>
					<div class="col-md-4 d-flex justify-content-center pagination pagination-sm">
						{{$categories->links()}}
					</div>
				</div>
				
				<table class="table text-center">
				<thead class="thead text-light">
					<tr>
						<th class="thCatImg">Category</th>
						<th class="thCatName">Name</th>
						<th>Status</th>
						<th>Available</th>
						<th class="thCatUse">In Use</th>
						<th>Total</th>
						<th>Actions</th>
					</tr>
				</thead>

				<tbody>
					@foreach($categories as $category)
						<tr>
							<td><div class="mx-auto imgCat">
								<img src="{{asset($category->img_Path)}}" class="card-img-top">
							</div></td> 
							<td><a href="/categories/{{$category->id}}">{{$category->name}}</a></td>
							<td>
								@if($category->isActive == 1)
									<span class="text-success"> <strong>{{"Active"}}</strong></span>
								@else
									<span class="text-danger"><strong>{{"Inactive"}}</strong></span>		
								@endif
							</td>
							<td>{{$isavailable[$loop->iteration]}}</td>
							<td>{{$isunavailable[$loop->iteration]}}</td>
							<td>{{$category->assets->count()}}</td>
							<td>
								<div class="dropdown mr-1">
								    <button type="button" class="btn btn-primary dropdown-toggle" id="dropdownMenuOffset" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="10,20">
								      Menu
								    </button>
								    <div class="dropdown-menu actionDropDown" aria-labelledby="dropdownMenuOffset">
								      {{-- <a href="/categories/{{$category->id}}/edit" class="dropdown-item">Edit</a> --}}
								      <!-- Button trigger modal -->
								      <a type="button" data-id="{{$category->id}}" data-title="{{$category->name}}" data-description="{{$category->description}}" data-code_name="{{$category->code_name}}"
								      class="dropdown-item" data-toggle="modal" data-target="#editModal">
								        Edit
								      </a>
								      <a href="/categories/{{$category->id}}" class="dropdown-item">Asset Line</a>
								      <a type="button" data-id="{{$category->id}}" data-name="{{$category->name}}" data-description="{{$category->description}}" data-code_name="{{$category->code_name}}"
								      data-description="{{$category->description}}"
								      class="dropdown-item" data-toggle="modal" data-target="#addQtyModal">
								        Add Quantity
								      </a>
								      <form method="POST" action="/categories/{{$category->id}}">
								      	@csrf
								      	@method('DELETE')
								      	@if($category->isActive == 1 )
								      		<button type="submit"  class="dropdown-item">Deactivate</button>
								      	@else
								      		<button type="submit" class="dropdown-item">Reactivate</button>
								      	@endif
								      </form>
								    </div>
								  </div>
								
								
								
							</td>
						</tr>
					@endforeach
				</tbody>
			@else

				<div class="row">
					<div class="col-md-4">
						<h2>Categories</h2>
					</div>
					<div class="col-md-4 d-flex justify-content-center pagination pagination-sm">
						{{$categories->links()}}
					</div>
				</div>
				<table class="table text-center">
				<thead class="thead text-light">
					<tr>
						<th class="thCatImg">Category</th>
						<th>Name</th>
						<th>Description</th>
						<th>Available</th>
						<th>Actions</th>
					</tr>
				</thead>

				<tbody>
					@foreach($categories as $category)
						@if($isavailable[$loop->iteration] > 0 && $category->isActive == 1)
							<tr>
								<td><div class="mx-auto imgCat">
									<img src="{{asset($category->img_Path)}}" class="card-img-top">
								</div></td> 
								<td>{{$category->name}}</td>
								<td>{{$category->description}}</td>
								<td>{{$isavailable[$loop->iteration]}}</td>
								<td>
									<button class="btn btn-primary" type="submit" data-toggle="modal" data-target="#requestModal" data-id="{{$category->id}}" data-name="{{$category->name}}">Request</button>
								</td>
							</tr>
						@endif
					@endforeach
				</tbody>
			@endcan
		</div>
	</div>

	<!-- Modals -->
	{{-- Edit Modal --}}
	<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<form id="editForm" method="POST" action="" enctype="multipart/form-data">
			@csrf
			@method('PUT')
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title text-center" id="modalTitle"></h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        @include('categories.edit')
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="submit" class="btn btn-primary">
		        	Save Product
		        </button>
		      </div>
		    </div>
		  </div>
		</form>
	</div>
	{{-- End of Edit Modal --}}

	{{-- Quantity Modal --}}
		<div class="modal fade" id="addQtyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<form method="POST" action="/assets" enctype="multipart/form-data">
			  @csrf
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title text-center">Add Quantity</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			      	<input type="hidden" name="category[]" id="category_id" readonly>
			        <div class="form-group">
			        	<label for="category_id">Name:</label>
			        	<input class="form-control" id="name" readonly>
			        </div>
			        <div class="form-group">
			        	<label for="quantity">Quantity: </label>
			        	<input class="form-control" type="number" min="1" name="quantity[]" class="quantity" id="quantity" required>
			        </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <button type="submit" class="btn btn-primary">
			        	Add Asset
			        </button>
			      </div>
			    </div>
			  </div>
			</form>
		</div>

	{{-- End of Quantity Modal --}}

	{{-- Request Modal --}}
		<div class="modal fade" id="requestModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<form method="POST" action="/transactions" enctype="multipart/form-data">
				@csrf
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title text-center"></h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
		      		<div class="form-group">
		      			<input type="hidden" id="user_id"  name="user_id" value="{{Auth::user()->id}}" >
		      			<input type="hidden" id="category_id" name="category_id">
		      			<label for="name">Borrow Date: </label>
		      			<input class="form-control" type="date" name="borrow_date" id="borrow_date">
		      			<label for="description">Return Date: </label>
		      			<input class="form-control" type="date" name="return_date" id="return_date" readonly>
		      		</div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
			        <button type="submit" class="btn btn-secondary" id="requestButton" disabled>
			        	Request Asset
			        </button>
			      </div>
			    </div>
			  </div>
			</form>
		</div>

	{{-- End of Request Modal --}}

	<script src="{{asset('js/indexCat.js')}}"></script>
@endsection