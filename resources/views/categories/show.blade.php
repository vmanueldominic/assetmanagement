@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-8 offset-2">
			{{-- call the Gate defined in AuthServiceProvider --}}
			@can('isAdmin')
				<div class="row">
					<div class="col-8">
						<h2>{{$category->name}}</h2>
					</div>
					<div class="col-4 text-right">
						<a href="/categories" class="btn btn-info showCatBtn text-light"><< All Categories</a>	
					</div>
				</div>
				<table class="table text-center">
				<thead class="thead text-light">
					<tr>
						<th>Asset ID</th>
						<th>Serial Number</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($category->assets as $asset)
						@if($category->id == $asset->category_id)
							<tr>
								<td>{{$asset->id}}</td>
								<td><a href="/assets/{{$asset->id}}">{{$asset->serial_code}}</a></td>
								<td>
									@if($asset->isAvailable == 1)
										<span class="text-success">{{"Active"}}</span>
									@else
										<span class="text-danger">{{"Inactive"}}</span>
									@endif
								</td>
								<td>
									<div class="dropdown mr-1 text-center">
									    <button type="button" class="btn btn-primary dropdown-toggle" id="dropdownMenuOffset" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="10,20">
									      Menu
									    </button>
									    <div class="dropdown-menu" aria-labelledby="dropdownMenuOffset">
									      <a class="dropdown-item" href="/assets/{{$asset->id}}">History</a>
									      <form method="POST" action="/assets/{{$asset->id}}">
									      	@csrf
									      	@method('DELETE')
									      	@if($asset->isAvailable == 1)
									      		<button type="submit"  class="dropdown-item">Deactivate</button>
									      	@else
									      		<button type="submit" class="dropdown-item">Reactivate</button>
									      	@endif
									      </form>
									    </div>
									  </div>
									</td>
							</tr>
						@endif
					@endforeach
				</tbody>
			@endcan
		</div>
	</div>
@endsection