@extends('layouts.app')

@section('content')
	@can('isAdmin')
	<div class="col-8 offset-2">
		<div class="card-header card text-black  mb-3">
			<div style="width: 25%;" class="mx-auto ">
				<img src="{{asset($asset->category->img_Path)}}" class="card-img-top img-fluid">
			</div>
			<div class="card-body">
				<h4 class="card-title">Name: {{$asset->category->name}}</h4>
				<h4 class="card-title">Serial #: {{$asset->serial_code}}</h4>
				<h4 class="card-title">Status: 
					@if($asset->isAvailable == 1)
						<a class="btn btn-success text-white" disabled><strong>Active</strong></a>
					@else
						<a class="btn btn-danger text-white" disabled><strong>Inactive</strong></a>
					@endif
				</h4>
				<p class="card-text"> <strong>Description:</strong> {{$asset->category->description}}</p>
				{{-- button leading back to all assets --}}
				<a href="/categories/{{$asset->category_id}}" class="btn btn-info float-right"><< All Assets</a>
			</div>
		</div>
		<div>
				<h2>Asset History</h2>
				<table class="table text-center">
					<thead class="thead text-light">
						<tr>
							<th>Borrowed By</th>
							<th>Date Borrowed</th>
							<th>Date Returned</th>
							<th>Actual Return</th>
							<th>Status</th>
							<th>Damaged</th>
							<th>Comment</th>
						</tr>
					</thead>
					<tbody>
						@foreach($transactions as $transaction)
							<tr>
								<td>{{$transaction['user']->name}}</td>
								<td>{{$transaction['borrow_date']}}</td>
								<td>{{$transaction['return_date']}}</td>
								<td>{{$transaction['status']->name}}</td>
								<td>{{$transaction['updated_at']}}</td>
								<td>
									@if($transaction['damage'] == 1)
										<strong class="text-danger">Yes</strong>
									@else
										{{"No"}}
									@endif
								</td>
								<td>{{$transaction['comment']}}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
		</div>
	</div>
	@endcan

@endsection