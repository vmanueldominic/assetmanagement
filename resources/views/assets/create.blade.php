@extends('layouts.app')

@section('content')
	<form method="POST" action="/assets" enctype="multipart/form-data">
		@csrf
		{{-- start of Add Existing Category --}}
		<div class="row">
			<div class="col-8 offset-2">
				{{-- error checker --}}		
				<div>
					@if ($errors->any())
						<div class="alert alert-danger">
							<div>
								<button type="button" class="close" data-dismiss="alert">&times;</button>
							</div>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>			  
					@endif
				</div>
				{{-- end of error check --}}

				{{-- Create Nofications --}}
				@if(session('status') == "New Categories Added!")
					<div class="alert alert-success text-center">
						<strong>{{session('status')}}</strong>
					</div>
				@elseif(session('status') == "New asset(s) has been added.")
					<div class="alert alert-success text-center">
						<strong>{{session('status')}}</strong>
					</div>
				@endif
				{{-- End of Nofications --}}
				<table class="table table-bordered">
					<thead class="thead text-light">
						<tr class="text-center">
							<th>Name</th>
							<th>Add Quantity</th>
							<th><a href="#" id="addRow" class="text-primary"><i class="fa fa-plus-square" aria-hidden="true"></i></a>
							</th>
						</tr>
					</thead>
					<tbody id="newTRow">
						<tr>
							<td>
								<select class="form-control selectItemName" id="category_id[]" name="category[]" required>
									<option>Select a category</option>
									@if(count($categories) > 0)
										@foreach($categories as $category)
											<option value="{{$category->id}}">{{$category->name}}</option>
										@endforeach
									@endif
								</select>
							</td>
							<td>
								<input class="form-control" type="number" min="1" name="quantity[]" class="quantity" id="quantity[]" required>
							</td>
							<td class="text-center"><a href="#" class="btn btn-danger remove"><i class="fa fa-times" aria-hidden="true"></i></a></td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td style="border: none"></td>
							<td style="border: none"></td>
							<td style="border: none" class="text-center"><button type="submit" class="btn btn-primary">Submit</button></td>
						</tr>
					</tfoot>
				</table>
			</div>	
		</div>
	</form>


{{-- 			<div class="card">
					<div class="card-header card text-white bg-dark mb-3">
						<a class="card-link h3">
							Add Assets
						</a>
					</div>

						<div class="card-body">
							<form method="POST" action="/assets" enctype="multipart/form-data">
								@csrf
								<div class="form-group">
									<label for="category_id">Name:</label>
									<select class="form-control" id="category_id" name="category">
										<option>Select a category:</option>
										@if(count($categories) > 0)
											@foreach($categories as $category)
												<option value="{{$category->id}}">{{$category->name}}</option>
											@endforeach
										@endif
									</select>
								</div>
								<div class="form-group">
									<label for="quantity">Quantity: </label>
									<input class="form-control" type="number" min="1" name="quantity" class="quantity" id="quantity">
								</div>
								<button type="submit" class="btn btn-success float-right">Add Asset</button>
							</form>
							
						</div>
						
					</div> --}}
				{{-- end of Update Existing Category --}}
		</div>	
	</div>
	<script src="{{asset('js/addAsset.js')}}"></script>
	<script type="text/javascript">
		
	$('#addRow').on('click', function(){
		newAsset();
	
	})

	let index = 1;
	function newAsset() {
		let addRow = '<tr>'+
					'<td>'+
					'<select class="form-control selectItemName" id="category_id" name="category[]">'+
					'<option>Select a category:</option>'+
					'@if(count($categories) > 0)'+
					'@foreach($categories as $category)'+
					'<option value="{{$category->id}}">{{$category->name}}</option>'+
					'@endforeach'+
					'@endif'+
					'</select>'+
					'</td>'+
					'<td>'+
					'<input class="form-control" type="number" min="1" name="quantity[]" class="quantity" id="quantity" required>'+
					'</td>'+
					'<td class="text-center"><a href="#" class="btn btn-danger remove"><i class="fa fa-times" aria-hidden="true"></i></a></td>'+
					'</tr>';
		let rows = $('#newTRow tr');
		if (rows.length < 10) {
			$('#newTRow').append(addRow);
			index ++;	
		}else{
			alert("10 is the maximum rows");
		}
	}
	</script>
@endsection





			