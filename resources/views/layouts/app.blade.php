<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{"Asset Managent System"}}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script src="https://use.fontawesome.com/874dbadbd7.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="./css/app.css" rel="stylesheet">


    {{-- css bootstrap --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">



</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark navBG">
            <div class="container">
                @if(empty(Auth::user()->isAdmin))
                    <a class="navbar-brand " href="{{ url('/') }}">
                    {{"Asset Managent System"}}
                </a>
                @else
                    <a class="navbar-brand " href="{{ url('/home') }}">
                    {{"Asset Managent System"}}
                </a>
                @endif
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    @if(empty(Auth::user()) )
                        <ul class="navbar-nav mr-auto">
                            
                        </ul>
                    @elseif(Auth::user()->isAdmin == 0)
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item {{ Request::segment(1) === 'transactions' || Request::segment(1) === 'home' ? 'active' : '' }}">
                                <a class="nav-link" href="{{ url('/transactions' )}}">Transactions<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item {{ Request::segment(1) === 'categories' ? 'active' : '' }}">
                                <a class="nav-link" href="{{ url('/categories' )}}">Categories<span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    @elseif(Auth::user()->isAdmin == 1)
                        <ul class="navbar-nav mr-auto">
                            <li class="{{ Request::segment(1) === 'transactions' || Request::segment(1) === 'home'? 'active' : '' }}">
                                <a class="nav-link" href="{{ url('/transactions' )}}">Transactions<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item dropdown {{ Request::segment(2) == 'create' || Request::segment(1) == 'categories' ? 'active' : '' }}">
                                    <a class="nav-link dropdown-toggle" href="{{ url('/categories' )}}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Categories
                                    <span class="sr-only">(current)</span></a>
                                    <div class="dropdown-menu bg-dark navbarDropdown" aria-labelledby="navbarDropdownMenuLink">
                                      <a class="dropdown-item {{ Request::segment(2) == '' ? 'active' : '' }}" href="{{ url('/categories' )}}">All Assets<span class="sr-only">(current)</span></a>  
                                      <a class="dropdown-item {{ Request::segment(1) === 'categories' && Request::segment(2) === 'create' ? 'active' : '' }}" href="{{ url('/categories/create' )}}">Create Category<span class="sr-only">(current)</span></a>
                                      <a class="dropdown-item {{ Request::segment(1) === 'assets' && Request::segment(2) === 'create' ? 'active' : '' }}" href="{{ url('/assets/create' )}}">Update Quantity<span class="sr-only">(current)</span></a>
                                    </div>
                                  </li>
                        </ul>  
                    @endif
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle active" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right bg-dark navbarDropdown" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>
