@extends('layouts.app')

@section('content')
	<div class="row">
		<div class="col-8 offset-2">
			{{-- call the Gate defined in AuthServiceProvider --}}
			@can('isAdmin')
				{{-- Notifications from controller --}}
				@if(session('status') == "The request has been REJECTED.")
					<div class="alert alert-warning text-center">
						<strong>{{session('status')}}</strong>
					</div>
				@elseif(session('status') == "The asset has been returned DAMAGED.")
					<div class="alert alert-danger text-center">
						<strong>{{session('status')}}</strong>
					</div>
				@elseif(session('status') == "The asset has been returned SUCCESSFULLY!")
					<div class="alert alert-success text-center">
						<strong>{{session('status')}}</strong>
					</div>
				@elseif(session('status') == "The request has been APPROVED!")
					<div class="alert alert-success text-center">
						<strong>{{session('status')}}</strong>
					</div>
				@endif
				{{-- end of notification --}}
				
				{{-- error checker --}}
				<div>
					@if ($errors->any())
					    <div class="alert alert-danger">
						    <ul>
					            @foreach ($errors->all() as $error)
					               <li>{{ $error }}</li>
					            @endforeach
					        </ul>
				  	    </div>  
					@endif
				</div>
				{{-- end of error checker --}}

				<h2>Transactions Pending Approval</h2>
				<table class="table text-center">
					<thead class="thead text-light text-center">
						<tr>
							<th>User</th>
							<th>Product Line</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Actions</th>
						</tr>
					</thead>

					<tbody>
						@foreach($transactions as $transaction)
							<tr> 
								@if($transaction->status_id == 1)
									<td>{{$transaction->user->name}}</td>
									<td>{{$transaction->category->name}}</td>
									<td>{{$transaction->borrow_date}}</td>
									<td>{{$transaction->return_date}}</td>
									<td>
										<form method="POST" action="/transactions/{{$transaction->id}}" enctype="multipart/form-data">
											@csrf
											@method('PUT')

											<button type="submit" class="btn btn-success">
												Approve
											</button>							
										</form>
										<form method="POST" action="/transactions/{{$transaction->id}}">
											@csrf
											@method('DELETE')
											<button type="submit"  class="btn btn-danger">Reject</button>
										</form>
									</td>
								@endif
							</tr>			
						@endforeach
					</tbody>
				</table>


				<h2>Approved Transactions</h2>
				<table class="table text-center">
					<thead class="thead text-light">
						<tr>
							<th>User</th>
							<th>Transaction ID</th>
							<th>Category</th>
							<th>Asset Serial #</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Approved Date</th>
							<th>Damaged</th>
							<th>Action</th>
						</tr>
					</thead>

					<tbody>
						@foreach($transactions as $transaction)
							<tr> 
								@if($transaction->status_id == 2)
									<td>{{$transaction->user->name}}</td>
									<td>{{$transaction->id}}</td>
									<td>{{$transaction->category->name}}</td>
									<td>{{$transaction->asset->serial_code}}</td>
									<td>{{$transaction->borrow_date}}</td>
									<td>{{$transaction->return_date}}</td>
									<td>{{$transaction->updated_at}}</td>
									<form method="POST" action="/transactions/{{$transaction->id}}">
										<td>
										<div>
										   <label><input type="radio" id="isDamaged" name="isDamaged" value="true" checked>Yes</label>
										   <label><input type="radio" id="isDamaged" name="isDamaged" value="false" checked>No</label>
										</div>
										<div class="form-group">
								       		<label for="name">Comment: </label>
								       		<input class="form-control" type="text" name="comment" id="comment">
								       	</div>
										</td>
										<td>
											@csrf
											@method('PUT')
								       	<button type="submit" class="btn btn-primary">
								     		Returned
								    	</button>	 							
							      	</form>
									</td>
								@endif
							</tr>	
						@endforeach
					</tbody>
				</table>
				
				

				<h2>Completed Transactions</h2>
				<table class="table text-center">
					<thead class="thead text-light">
						<tr>
							<th>User</th>
							<th>Transaction ID</th>
							<th>Asset Serial #</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Status</th>
							<th>Approved Date</th>
							<th>Comments</th>
						</tr>
					</thead>

					<tbody>
						@foreach($transactions as $transaction)
							<tr> 
								@if($transaction->status_id != 1)
									<td>{{$transaction->user->name}}</td>
									<td>{{$transaction->id}}</td>
									<td>
										@if($transaction->asset_id == '')
										{{"N/A"}}
										@else
										{{$transaction->asset->serial_code}}
										@endif
									
									</td>
									<td>{{$transaction->borrow_date}}</td>
									<td>{{$transaction->return_date}}</td>
									<td>{{$transaction->status->name}} </td>
									<td>{{$transaction->updated_at}}</td>
									<td>
										@if($transaction->damage == 1)
											"Damaged upon return" "Reason:{{$transaction->comment}}"
										@else
											{{$transaction->comment}}
										@endif
									</td>
								@endif
							</tr>			
						@endforeach
					</tbody>
				</table>
			@else


				<h2>Transactions Pending Approval</h2>
				{{-- Notifications from Controller --}}
				@if(session('status') == "Your request has been submitted")
					<div class="alert alert-success text-center">
						<strong>{{session('status')}}</strong>
					</div>
				@elseif(session('status') == "You cancelled your request.")
					<div class="alert alert-warning text-center">
						<strong>{{session('status')}}</strong>
					</div>
				@endif
				{{-- End of notifications--}}
				<table class="table text-center">
					<thead class="thead text-light">
						<tr>
							<th>Product Line</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Actions</th>
						</tr>
					</thead>

					<tbody>
						@foreach($transactions as $transaction)
							<tr> 
								@if($transaction->status_id == 1)
								<td>{{$transaction->category->name}}</td>
								<td>{{$transaction->borrow_date}}</td>
								<td>{{$transaction->return_date}}</td>
								<td>
									<form method="POST" action="/transactions/{{$transaction->id}}">
										@csrf
										@method('DELETE')
										<button type="submit"  class="btn btn-danger">Cancel</button>
									</form>
								</td>
								@endif
							</tr>			
						@endforeach
					</tbody>
				</table>

				<h2>Approved Transasctions</h2>
				<table class="table text-center">
						<thead class="thead text-light text-center">
							<tr>
								<th>Transaction ID</th>
								<th>Category</th>
								<th>Asset Serial #</th>
								<th>Borrow Date</th>
								<th>Return Date</th>
								<th>Approved Date</th>
							</tr>
						</thead>

						<tbody>
							@foreach($transactions as $transaction)
								<tr> 
									@if($transaction->status_id == 2)
										<td>{{$transaction->id}}</td>
										<td>{{$transaction->category->name}}</td>
										<td>{{$transaction->asset->serial_code}}</td>
										<td>{{$transaction->borrow_date}}</td>
										<td>{{$transaction->return_date}}</td>
										<td>{{$transaction->updated_at}}</td>
									@endif
								</tr>	
							@endforeach
					</tbody>
				</table>

				<h2>Completed Transactions</h2>
				<table class="table text-center">
				{{-- check if a session flash cariable containing a notification message is set --}}
					{{-- catalogue vie for non-admin users --}}	
					<thead class="thead text-light">
						<tr>
							<th>Transaction ID</th>
							<th>Asset Serial #</th>
							<th>Borrow Date</th>
							<th>Return Date</th>
							<th>Status</th>
							<th>Approved Date</th>
							<th>Comments</th>
						</tr>
					</thead>

					<tbody>
						@foreach($transactions as $transaction)
							<tr> 
								@if($transaction->status_id != 1)
									<td>{{$transaction->id}}</td>
									<td>
										@if($transaction->asset_id == '')
										{{"N/A"}}
										@else
										{{$transaction->asset->serial_code}}
										@endif
									
									</td>
									<td>{{$transaction->borrow_date}}</td>
									<td>{{$transaction->return_date}}</td>
									<td>{{$transaction->status->name}}</td>
									<td>{{$transaction->updated_at}}</td>
									<td>
										@if($transaction->damage == 1)
											{{"Damaged when return "}} {{$transaction->comment}}
										@else
											{{$transaction->comment}}
										@endif
									</td>
								@endif
							</tr>			
						@endforeach
				</tbody>
			</table>
			@endcan
		</div>
	</div>

@endsection